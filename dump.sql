-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: kiv-web-sem
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acl`
--

DROP TABLE IF EXISTS `acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl` (
  `id_acl` int(11) NOT NULL AUTO_INCREMENT,
  `id_route` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `action` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_route`,`id_role`,`action`),
  UNIQUE KEY `id_acl` (`id_acl`),
  KEY `fk_routes_has_role_role1_idx` (`id_role`),
  KEY `fk_routes_has_role_routes1_idx` (`id_route`),
  CONSTRAINT `fk_routes_has_role_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_routes_has_role_routes1` FOREIGN KEY (`id_route`) REFERENCES `routes` (`id_route`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl`
--

LOCK TABLES `acl` WRITE;
/*!40000 ALTER TABLE `acl` DISABLE KEYS */;
INSERT INTO `acl` VALUES (1,1,1,''),(2,1,2,''),(3,1,3,''),(4,2,1,'finishSession'),(5,2,2,'finishSession'),(6,2,3,''),(7,2,3,'signIn'),(8,3,3,''),(9,3,3,'signUp'),(12,4,2,''),(13,4,2,'propose'),(14,5,1,''),(15,5,2,''),(16,5,3,''),(17,6,1,''),(18,6,2,''),(19,6,3,''),(20,7,1,''),(21,7,1,'add'),(22,7,1,'delete'),(24,14,1,''),(29,14,1,'approve'),(30,14,1,'disapprove'),(31,14,1,'assign'),(35,14,1,'addReviewer'),(36,14,1,'removeReview'),(41,17,2,'download'),(42,17,2,''),(43,17,2,'delete'),(46,17,2,'edit'),(48,1,1,'download'),(49,1,3,'download'),(50,1,2,'download'),(52,1,6,''),(53,5,6,''),(54,6,6,''),(55,1,6,'download'),(56,2,6,'finishSession'),(57,16,6,''),(58,16,6,'download'),(59,16,6,'save'),(60,18,1,''),(61,18,1,'enable'),(62,18,1,'disable'),(63,18,1,'setRole');
/*!40000 ALTER TABLE `acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_author` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `abstract` text COLLATE utf8_czech_ci NOT NULL,
  `file_path` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `accepted` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_article`),
  UNIQUE KEY `id_article_UNIQUE` (`id_article`),
  KEY `fk_article_user1_idx` (`id_user_author`),
  CONSTRAINT `fk_article_user1` FOREIGN KEY (`id_user_author`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,1,'Testovací článek','abstraktní abstrakt ze všech nejabstraktnější, asi jako obstraktní třída bez implementovaných metod, takže vlastně rozhraní.','5c0420856a6eakniska-karla-kryla.pdf',NULL),(3,1,'Testovací článek 2','test lorem ipsum dolor sit amet test lorem ipsum dolor sit amet test lorem ipsum dolor sit amet test lorem ipsum dolor sit amet test lorem ipsum dolor dddddddddddddddddddddddddddddddddddddddd sqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqsum dolor sit amet ','2018-12-07listina_zpas.pdf',NULL),(4,1,'Intel Working On Open-Sourcing The FSP','Intel\'s Architecture Day on Tuesday was delightfully filled with an overwhelming amount of valuable hardware information, but Intel\'s software efforts were also briefly touched on too.','2018-12-07testxxxqqqkniska-karla-kryla.pdf',1),(5,3,'Spytihněvův algoritmus','Monte carlo algoritmus, který dokáže vyřešit problém obchodního cestujícího v O(1)','2018-12-07sss.pdf',NULL),(6,6,'Množiny a relace ss','Máte před sebou pracovní verzi textu k přednášce Diskrétní matematika na Západočeské univerzitě v Plzni. Vlastní text by měl být již víceméně v konečné podobě; zatím chybí výsledky a nápovědy ke cvičením a rovněž rejstřík. Přibudou patrně i další cvičení a možná několik rozšiřujících pasáží. Vydání definitivní verze skript (i na papíře) je plánováno na začátek roku 2004. ','1544630572-1.pdf',NULL),(7,6,'testovací článek 2222','testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací článek 2222testovací ','1544765298-6-kniska-karla-kryla.pdf',NULL);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id_review` int(11) NOT NULL AUTO_INCREMENT,
  `id_article` int(11) NOT NULL,
  `id_user_reviewer` int(11) NOT NULL,
  `content` text COLLATE utf8_czech_ci,
  `relevance` tinyint(4) DEFAULT NULL,
  `grammar` tinyint(4) DEFAULT NULL,
  `correctness` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_review`),
  UNIQUE KEY `id_review_UNIQUE` (`id_review`),
  KEY `fk_review_article1_idx` (`id_article`),
  KEY `fk_review_user1_idx` (`id_user_reviewer`),
  CONSTRAINT `fk_review_article1` FOREIGN KEY (`id_article`) REFERENCES `articles` (`id_article`) ON DELETE CASCADE,
  CONSTRAINT `fk_review_user1` FOREIGN KEY (`id_user_reviewer`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,1,1,'lorem ipsum dolor sit amet',1,2,3),(10,1,2,'Moc se mi to líbí, určitě bych publikoval!',4,5,3),(12,1,4,NULL,NULL,NULL,NULL),(20,5,2,'testovací recenze lorem ipsum dolor sit amet',3,2,5),(21,5,5,'testovací recenze Spyťova algoritmu',4,5,4),(22,4,2,'Je to odpad, nebrat...',1,2,5),(25,4,5,',msfbna dfsf jadf dsfahds fdsaj ',4,3,4),(27,4,4,'testovací recenze qqqqqq',3,5,2),(28,7,2,NULL,NULL,NULL,NULL),(29,7,4,NULL,NULL,NULL,NULL),(30,6,2,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `string_id` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id_role`),
  UNIQUE KEY `id_role_UNIQUE` (`id_role`),
  UNIQUE KEY `string_id_UNIQUE` (`string_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(3,'guest'),(6,'reviewer'),(2,'user');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routes` (
  `id_route` int(11) NOT NULL AUTO_INCREMENT,
  `string_id` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `model` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  `view` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  `controller` varchar(45) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id_route`),
  UNIQUE KEY `id_route_UNIQUE` (`id_route`),
  UNIQUE KEY `string_id` (`string_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
INSERT INTO `routes` VALUES (1,'index','readonlyGeneral','index','index'),(2,'signin','user','signIn','signIn'),(3,'signup','user','signUp','signUp'),(4,'propose','article','propose','propose'),(5,'404','readonlyGeneral','err404','index'),(6,'403','readonlyGeneral','err403','index'),(7,'editacl','acl','editAcl','editAcl'),(14,'proposals','proposal','proposal','proposal'),(15,'assignProposals','proposal','assignProposal','proposal'),(16,'review','review','review','review'),(17,'viewOwn','article','ownArticle','ownArticle'),(18,'manageUsers','user','manageUsers','manageUsers'),(19,'editUsers',NULL,NULL,NULL),(20,'signinn',NULL,NULL,NULL);
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) NOT NULL DEFAULT '2',
  `password` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `surname` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `mail` varchar(45) COLLATE utf8_czech_ci NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `id_user_UNIQUE` (`id_user`),
  KEY `fk_users_role1_idx` (`id_role`),
  CONSTRAINT `fk_users_role1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'$2y$10$aDTgtm5e6vRRhmlj0fQTTeil378rLAmk7/ZtGko8.w0WrEpYMDB.S','Jarda','Lesák','lesak@les.pes',1),(2,6,'$2y$10$yAb5UQi5.Gmh/pNNQJ6bsuxHMmIM3X9Bk9N9jCccSh67PokKeS5Fu','Prokop','Buben','prokopava@bubny.cz',1),(3,2,'$2y$10$43FexiYYTkupiZ7CoZhVDe5f5t8mlaIv5uYOTWawRhJ7Wx56SnI6C','Spytihněv','Chytrý','spytan@yolo.cz',1),(4,6,'$2y$10$WbabEiLNdbbRxY0mtfT/Tea3gBpmJXA90rAen.46TXbgHASLX5Y6u','Jolanda','Jolandová','yolanda@barrandov.cz',1),(5,6,'$2y$10$/cFQdgz78c4QfJ5xLJciK.eychofTmlriphltH6gSoRH9eZtZUOW2','Bivoj','Kaňour','reviewer@review.com',1),(6,6,'$2y$10$878JzFYryVXaOcq2H4GbOeSm1ssUcvObkEl18JqGPEVpCeGrC3vC2','Tester','Testovací','tester@test.cz',1),(7,2,'$2y$10$Qvj3opCOW7WaBIDhX0J2JeJ3syWujJXbN4wf4sl7EgHCfDxmvanXm','Mistr','Sláma','mistr@ministr.gov',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-16 17:44:57
