<?php

/**
 * This is index file.
 *
 * @author Martin Procházka
 * @since 1.12.2018
 */

require_once 'vendor/autoload.php';
require_once 'app/model/Router.php';
require_once 'app/model/User.php';
require_once 'app/controller/ActionResult.php';
require_once 'app/model/Role.php';
require_once 'app/controller/FrontController.php';

session_start();

$route = $_GET['route'] ?? 'index';
$_CORE = new Core();

// pull undisplayed results from previous route
$actionResults = $_SESSION['action_results'] ?? [];
unset($_SESSION['action_results']);

// set user
$isLogged = !is_null($_CORE->getUser());
$user = $_CORE->getUser();

// init twig
$twig_loader = new Twig_Loader_Filesystem("app/view/templates");
$twig = new Twig_Environment($twig_loader, ['debug'=>true]);
$twig->addExtension(new Twig_Extension_Debug());

try {
    // prepare environment for the twig template
    $filling = [
        "user"    => $user,
        "role"    => $_CORE->getUserRole(),
        "logged"  => $isLogged,
        "route"   => $route,
        "acl"     => new Acl(false),
    ];
    try {
        // FrontController init
        $frontController = new FrontController(new Router(), $route, $twig);
        if (isset($_GET['action'])){
            // do action if needed
            $frontController->doAction($_GET['action']);
        }
        // render results undisplayed results and results from current action
        $results = $twig->render("err_messages.html", ['actions' => array_merge($actionResults, $frontController->getResults())]);
        // if this is an ajax call then we're done
        if (isAjax()){
            echo $results;
            die;
        }
        // otherwise add results to environment
        $filling["center"] = $frontController->render();
    } catch (Exception $e){
        if (isAjax()){
            // exception thrown while resolving ajax call => render the message
            echo $twig->render("err_messages.html",
                ['actions' => [new ActionResult('Error '. $e->getMessage(), Severity::ERROR)]]);;
            die;
        } else if (ctype_digit($e->getMessage())) {
            // exception while routing redirect to according route
            Core::redirect($e->getMessage());
        }
    }
    // render the page
    $filling["errs"] = $results;
    echo $twig->render("top_level_template.html", $filling);
} catch (Twig_Error $e) {
    echo "failed to load template :(";
}

/**
 * Checks if this request is ajax and
 * returns true if so and false otherwise.
 *
 * @return bool true if ajax otherwise false
 */
function isAjax() : bool {
    return isset($_POST['ajax_call']) && $_POST['ajax_call'] == 'true';
}

