<?php

/**
 * Class Role
 *
 * This class represents a Role.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
class Role {

    private $id_role;
    private $string_id;

    /**
     * Fetches role from database by its id
     * and returns it if found or null otherwise.
     *
     * @param int $id
     * @return null|Role
     */
    public static function fetch(int $id) : ?Role {
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM role WHERE id_role = ?");
        $stmt->execute([$id]);

        return $stmt->fetchObject(self::class) ?: null;
    }

    /**
     * Fetches all roles and returns them in
     * an array of Role class refs.
     *
     * @return array
     */
    public static function fetchAll() : array {
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM role");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, self::class)
            ?: [];
    }

    /**
     * Fetches role by its string representation
     * and returns it if found or null otherwise.
     *
     * @param string $name
     * @return null|Role
     */
    public static function fetchByName(string $name) : ?Role{
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM role WHERE string_id = ?");
        $stmt->execute([$name]);

        return $stmt->fetchObject(self::class) ?: null;
    }

    /**
     * Returns id of this role.
     *
     * @return int
     */
    public function getId() : int {
        return $this->id_role;
    }

    /**
     * Alias for __toString()
     *
     * @deprecated
     * @return string
     */
    public function getString() : string {
        return $this->__toString();
    }

    /**
     * Returns string representation of this Role.
     *
     * @return string
     */
    public function __toString() : string {
        return $this->string_id;
    }
}