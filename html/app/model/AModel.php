<?php

include_once "PDOFactory.php";
include_once "Article.php";

/**
 * Class AModel
 *
 * This is abstract model which all models
 * should extend.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
abstract class AModel {

    private $errors;
    protected $pdo;
    protected $core;

    /**
     * AModel constructor.
     *
     * Sets error list to empty array
     * and prepares pdo.
     */
    public function __construct() {
        global $_CORE;
        $this->core = &$_CORE;
        $this->pdo = PDOFactory::get();
        $this->errors = [];
    }

    /**
     * Adds error specified by message and its
     * severity (ERROR by default). These severities
     * map 1:1 to bootstrap colors. Some predefined
     * constants can be found in {@link Severity}
     * class.
     *
     * @param string $message error message
     * @param string $severity error severity
     */
    protected function addErr(string $message, string $severity = Severity::ERROR) : void{
        array_push($this->errors, [$message, $severity]);
    }

    /**
     * Returns all errors as array of {@link ActionResult}
     * instances.
     *
     * @return array of ActionResults
     */
    public function getErrs() : array {
        $array = [];
        foreach ($this->errors as $err){
            array_push($array, new ActionResult($err[0], $err[1], __FUNCTION__));
        }
        return $array;
    }

    /**
     * Checks if any errors occurred and returns
     * true if so and false otherwise.
     *
     * @return bool true if errors occurred, false otherwise
     */
    public function hasErrs() : bool {
        return sizeof($this->errors) != 0;
    }
}