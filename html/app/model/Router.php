<?php

/**
 * Class Router
 *
 * This class' purpose is fetching routes
 * from database based on routes string
 * representation.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
class Router {

    /**
     * Fetches route by its string representation
     * and returns it as reference to Route instance
     * or null if not found.
     *
     * @param string $route
     * @return null|Route
     */
    private function fetchByString(string $route) : ?Route {
        $stmt = PDOFactory::get()->prepare(
            'SELECT string_id,controller,view,model FROM routes WHERE string_id = ?'
        );
        $stmt->execute([$route]);
        return $stmt->fetchObject(Route::class) ?: null;
    }

    /**
     * This is safer version of fetchByString
     * that means it is guaranteed to return
     * valid Route. If route is not in database
     * then 404 error route is returned.
     *
     * @param string $route
     * @return Route
     */
    public function getRoute(string $route) : Route {
        return  $this->fetchByString($route) ?? $this->fetchByString('404');
    }
}

/**
 * Class Route
 *
 * This class represents a Route.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
class Route {

    private $string_id;
    private $model;
    private $view;
    private $controller;

    /**
     * Route constructor.
     *
     * Private, maps database strings to
     * actual object names.
     */
    private function __construct() {
        $this->model = ucfirst($this->model) . 'Model';
        $this->view = ucfirst($this->view) . 'View';
        $this->controller = ucfirst($this->controller) . 'Controller';
    }

    /**
     * Returns name of Model class.
     *
     * @return string
     */
    public function getModel() : string {
        return $this->model;
    }

    /**
     * Returns name of View class.
     *
     * @return string
     */
    public function getView() : string {
        return $this->view;
    }

    /**
     * Returns name of Controller class.
     *
     * @return string
     */
    public function getController() : string {
        return $this->controller;
    }

    /**
     * Returns name of the route.
     *
     * @return string
     */
    public function __toString() : string {
        return $this->string_id;
    }
}