<?php

/**
 * Class AclModel
 *
 * This class is abstraction layer above Acl
 * object. It's purpose is to extend its features
 * and add input validation to be safely used by
 * controllers.
 *
 * @since 4.12.2018
 * @author Martin Procházka
 */
class AclModel extends AModel {

    /**
     * Fetches Acl and returns it as
     * array of Acl object instances.
     *
     * @return array of Acl
     */
    public function getAcl() : array {
        return Acl::fetchAll();
    }

    /**
     * Updates route and role of Ac rule
     * with input validation, expected keys are:
     *
     * int      => id of ac
     * string   => role name
     * string   => action name
     *
     * @param array $values
     */
    public function updateAc(array $values) : void {
        if (!isset($values, $values['id'], $values['role'], $values['route'], $values['action'])){
            $this->addErr('Všechna pole jsou povinná');
            return;
        }

        if (!$this->addRouteIfMissing($values['route'])){
            $this->addErr('Vytvořena neexistující trasa '.$values['route'], Severity::INFO);
        } if (!$this->addRoleIfMissing($values['role'])){
            $this->addErr('Vytvořena neexistující role '.$values['role'], Severity::INFO);
        }

        $stmt = $this->pdo->prepare("
            UPDATE acl 
                SET id_route = (SELECT id_route FROM routes WHERE string_id = ?), 
                    id_role = (SELECT id_role FROM role WHERE string_id = ?),
                    action = ?
                WHERE id_acl = ?
        ");
        $stmt->execute([$values['route'], $values['role'], $values['action'], $values['id']]);
    }

    /**
     * Adds ac rule to acl with input
     * validation. Expected keys are:
     *
     * int      => role
     * string   => route name
     * string   => action name
     *
     * @param array $values
     */
    function addAc(array $values) : void {
        if (!isset($values, $values['role'], $values['route'], $values['action'])){
            $this->addErr('Všechna pole jsou povinná');
            return;
        }

        if (!$this->addRouteIfMissing($values['route'])){
            $this->addErr('Vytvořena neexistující trasa '.$values['route'], Severity::INFO);
        } if (!$this->addRoleIfMissing($values['role'])){
            $this->addErr('Vytvořena neexistující role '.$values['role'], Severity::INFO);
        }

        $this->pdo->prepare("
            INSERT INTO acl (id_route, id_role, action) 
            VALUES (
                (SELECT id_route FROM routes WHERE string_id = ?), 
                (SELECT id_role FROM role WHERE string_id = ?),
                ?
            )
        ")->execute([$values['route'], $values['role'], $values['action']]);
    }

    /**
     * Takes name of route and checks if it exists.
     * If it does not exist then it's created and
     * false is returned otherwise true is returned.
     *
     * @param string $newRoute route name
     * @return False if route was missing, true otherwise
     */
    function addRouteIfMissing(string $newRoute) : bool {
        $stmt = $this->pdo->prepare("SELECT id_route FROM routes WHERE string_id = ?");
        $stmt->execute([$newRoute]);
        if ($stmt->fetch() === false){
            $stmt = $this->pdo->prepare("INSERT INTO routes (string_id) VALUES (?)");
            $stmt->execute([$newRoute]);
            return false;
        }
        return true;
    }

    /**
     * Takes name of role and checks if it exists.
     * If it does not exist then it's created and
     * false is returned otherwise true is returned.
     *
     * @param string $newRole role name
     * @return False if role was missing, true otherwise
     */
    function addRoleIfMissing(string $newRole) : bool {
        $stmt = $this->pdo->prepare("SELECT id_role FROM role WHERE string_id = ?");
        $stmt->execute([$newRole]);
        if ($stmt->fetch() === false){
            $stmt = $this->pdo->prepare("INSERT INTO role (string_id) VALUES (?)");
            $stmt->execute([$newRole]);
            return false;
        }
        return true;
    }

    /**
     * Deletes ac rule by its id.
     *
     * @param int $id
     */
    public function deleteById(int $id) {
        $stmt = $this->pdo->prepare("
            DELETE FROM acl
            WHERE id_acl = ?
        ");
        $stmt->execute([$id]);
    }
}