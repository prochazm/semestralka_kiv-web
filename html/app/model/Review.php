<?php

/**
 * Class Review
 *
 * This class represents actual review.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class Review {

    private $id_review;
    private $id_article;
    private $id_user_reviewer;
    private $content;
    private $relevance;
    private $grammar;
    private $correctness;

    private $reviewer;
    private $article;

    /**
     * Review constructor.
     *
     * Private, use factory method.
     */
    private function __construct() {}

    /**
     * Fetches Reviews by reviewer's id and
     * returns them as array of Reviews
     *
     * @param int $reviewerId
     * @return array
     */
    public static function fetchByReviewer(int $reviewerId) : array {
        $stmt = PDOFactory::get()->prepare("
            SELECT * FROM reviews
            WHERE id_user_reviewer = ?
        ");
        $stmt->execute([$reviewerId]);

        return $stmt->fetchAll(PDO::FETCH_CLASS, self::class)
            ?: [];
    }

    /**
     * Fetches Reviews by id of article and
     * returns them as array of Reviews.
     *
     * @param int $id
     * @return array
     */
    public static function fetchByArticleId(int $id) : array {
        $stmt = PDOFactory::get()->prepare("
            SELECT * FROM reviews
            WHERE id_article = ?
        ");
        $stmt->execute([$id]);

        return $stmt->fetchAll(PDO::FETCH_CLASS, self::class)
            ?: [];
    }

    /**
     * Getter returns Reviewer for this review.
     * Uses lazy init.
     *
     * @return User
     */
    public function getReviewer() : User {
        if (!isset($this->reviewer)){
            $stmt = PDOFactory::get()->prepare("
                SELECT * FROM users
                WHERE id_user = ?;
            ");
            $stmt->execute([$this->id_user_reviewer]);
            $this->reviewer = $stmt->fetchObject(User::class);
        }
        return $this->reviewer;
    }

    /**
     * Getter returns Article associated with this
     * Review instance. Uses lazy init.
     *
     * @return Article
     */
    public function getArticle() : Article {
        if (isset($this->article)){
            return $this->article;
        }
        $stmt = PDOFactory::get()->prepare("
            SELECT * FROM articles
            WHERE id_article = ?;
        ");
        $stmt->execute([$this->id_article]);
        return ($this->article = $stmt->fetchObject(Article::class));
    }

    /**
     * Returns true if review is empty which means
     * there is no content and rating set yet and
     * false otherwise.
     *
     * @return bool
     */
    public function isEmpty() : bool {
        return !isset($this->content)
            && !isset($this->relevance)
            && !isset($this->correctness)
            && !isset($this->grammar);
    }

    /**
     * Returns id of this Review instance.
     *
     * @return int
     */
    public function getId() : int {
        return $this->id_review;
    }

    /**
     * Returns id of Article of this Review
     * instance.
     *
     * @return int
     */
    public function getIdArticle() : int {
        return $this->id_article;
    }

    /**
     * Returns id of User of this Review
     * instance.
     *
     * @return int
     */
    public function getIdUserReviewer() : int {
        return $this->id_user_reviewer;
    }

    /**
     * Returns content of this Review instance.
     *
     * @return string
     */
    public function getContent() : ?string {
        return $this->content;
    }

    /**
     * Returns relevance rating of this Review
     * instance or null if unset.
     *
     * @return int|null
     */
    public function getRelevance() : ?int {
        return $this->relevance;
    }

    /**
     * Returns grammar rating of this Review
     * instance or null if unset.
     *
     * @return int|null
     */
    public function getGrammar() : ?int {
        return $this->grammar;
    }

    /**
     * Returns correctness rating of this Review
     * instance or null if unset.
     *
     * @return int|null
     */
    public function getCorrectness() : ?int {
        return $this->correctness;
    }

}