<?php

/**
 * Class PDOFactory
 *
 * This is factory/configuration class for PDO
 * which is lazily initialized and then recycled
 * for subsequent calls.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class PDOFactory {

    // these are self explanatory
    private const CONNECTION_STRING = "mysql:host=%s;dbname=%s;charset=%s";
    private const HOST = 'localhost';
    private const DATABASE = 'kiv-web-sem';
    private const USER = 'web';
    private const CHARSET = 'UTF8';
    private const PASSWD = 'password';

    /**
     * This is actual reference to the PDO instance
     *
     * @var PDO
     */
    private static $pdo;

    /**
     * PDOFactory constructor.
     *
     * This is private as this is factory class.
     */
    private function __construct(){}

    /**
     * Lazy initializes and returns the PDO.
     *
     * @return PDO
     */
    public static function get() : PDO {
        if (!isset(self::$pdo)){
            $dsn = sprintf(self::CONNECTION_STRING, self::HOST, self::DATABASE, self::CHARSET);
            $options = [
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES   => false,
            ];

            self::$pdo = new PDO($dsn, self::USER, self::PASSWD, $options);
        }

        return self::$pdo;
    }
}