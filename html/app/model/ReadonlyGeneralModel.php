<?php

/**
 * Class ReadonlyGeneralModel
 *
 * Simple readonly model for articles.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class ReadonlyGeneralModel extends AModel {

    /**
     * Returns user name.
     *
     * @return string
     */
    function getUserName() : string {
        return $this->core->getUser();
    }

    /**
     * Fetches all accepted articles and
     * returns them as array of Articles.
     *
     * @return array
     */
    function getArticles() : array {
        $stmt = $this->pdo->prepare("
            SELECT * FROM articles 
            WHERE accepted = 1;
        ");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class)
            ?: [];
    }

    /**
     * Fetches Article by its id.
     *
     * @param int $id
     * @return null|Article
     */
    function getArticle(int $id) : ?Article {
        $stmt = $this->pdo->prepare("
            SELECT * FROM articles
            WHERE id_article = ?
        ");
        $stmt->execute([$id]);

        return $stmt->fetchObject(Article::class) ?: null;
    }
}