<?php

/**
 * Class ArticleModel
 *
 * This class can do some advanced magic
 * with articles.
 *
 * @since 2.12.2018
 * @author Martin Procházka
 */
class ArticleModel extends AModel {

    /**
     * Absolute path to upload directory
     *
     * @var string
     */
    private const UPLOAD_DIR = '/var/www/uploads/';


    /**
     * Performs validation of array values and
     * returns true if they are suitable for
     * persisting as new article and false otherwise.
     *
     * If $edit is set to true then uniqueness
     * constraint for article title is skipped
     * as you obviously will submit non-unique
     * article name while editing it.
     *
     * applied constraints are:
     * -> sizeof title in (8;64)
     * -> sizeof abstract in (64;1024)
     * -> title is unique
     *
     * @param $values
     * @param bool $edit
     * @return bool true if constraints satisfied otherwise false
     */
    private function validateArticle($values, $edit = false) : bool {
        if (!isset($values, $values['title'], $values['abstract'])){
            $this->addErr('Titulek a abstrakt jsou povinné položky, vyplňte je.');
        } if (strlen($values['title']) < 8) {
            $this->addErr('Název titulku je příliš krátký, minimum je 8 znaků.');
        } if (strlen($values['title']) > 64) {
            $this->addErr('Název titulku je příliš dlouhý, maximum je 64 znaků.');
        } if (strlen($values['abstract']) < 64){
            $this->addErr('Abstrakt je příliš krátký, minimum je 64 znaků.');
        } if (strlen($values['abstract']) > 1024){
            $this->addErr('Abstrakt je příliš krátký, maximum je 1024 znaků.');
        } if (!$edit && !is_null($this->getByTitle($values['title']))){
            $this->addErr('Článek se stejným titulkem již existuje.');
        }
        return !$this->hasErrs();
    }

    /**
     * Performs validation of file. If file is
     * not suitable for upload then false is
     * returned otherwise returns true.
     *
     * applied constraints are:
     * -> type is pdf
     * -> no transmission error occurred
     * -> size is less than 2 megs
     *
     * @param $file
     * @return bool true if constraints satisfied otherwise false
     */
    private function validateFile($file) : bool {
        if (!isset($file)){
            $this->addErr('Soubor je povinná položka, vyplňte jej.');
        } if ($file['type'] != 'application/pdf'){
            $this->addErr('Akceptovány jsou pouze pdf soubory.');
        } if ($file['error'] != 0){
            $this->addErr('Při přenosu došlo k chybě.');
        } if ($file['size'] > 2097152){
            $this->addErr('Soubor je příliš veliký, maximální velikost je 2MiB.');
        }
        return !$this->hasErrs();
    }

    /**
     * Tries to upload file to {@link UPLOAD_DIR},
     * name is constructed in format of TS-ID-NAME
     * where TS is for timestamp, ID is for user id
     * (which is passed as argument) and NAME is
     * the original file name.
     *
     * If failure occurs while moving the file then
     * error is set and null returned. In case of
     * success name of the new file is returned.
     *
     * @param array $file
     * @param int $id
     * @return null|string null if fail otherwise file name
     */
    private function uploadFile(array $file, int $id) : ?string {
        $fileName = time() . "-$id-" . basename($file["name"]);
        $filepath = self::UPLOAD_DIR . $fileName;

        if (!move_uploaded_file($file['tmp_name'], $filepath)) {
            $this->addErr('Nastala chyba při nahrávání souboru.');
            return null;
        }
        return $fileName;
    }

    /**
     * Performs validation and inserts article
     * defined by $values array. Also title and
     * abstract are stripped of newlines as they
     * do some trouble when injecting them to the
     * template.
     *
     * @param array $values
     */
    public function add(array $values) : void {
        if (!$this->validateArticle($values) ||
            !$this->validateFile($values['file'])){
            return;
        }

        $fileName = $this->uploadFile($values['file'], $this->core->getUser()->getId());
        if (!isset($fileName)){
            return;
        }

        $stmt = $this->pdo->prepare(
            'INSERT INTO articles (id_user_author, title, abstract, file_path)'.
                      'VALUES (?,?,?,?)'
        );

        str_replace("\n", "", $values['abstract']);
        str_replace("\n", "", $values['title']);
        $id_user = $this->core->getUser()->getId();
        $stmt->execute([$id_user,$values['title'], $values['abstract'], $fileName]);
    }

    /**
     * Tries to fetch Article by title
     * and returns it if found, otherwise
     * null is returned.
     *
     * @param string $title
     * @return Article|null
     */
    public function getByTitle(string $title) : ?Article {
        $stmt = PDOFactory::get()->prepare("SELECT id_article FROM articles WHERE title = ?");
        $stmt->execute([$title]);

        return $stmt->fetchObject(Article::class) ?: null;
    }

    /**
     * Fetches articles for user defined by
     * id and returns them as array of Article
     * instances.
     *
     * @param int $idUser
     * @return array of Articles
     */
    public function getByUser(int $idUser) : array {
        $stmt = $this->pdo->prepare("
            SELECT * FROM articles
            WHERE id_user_author = ?;
        ");

        $stmt->execute([$idUser]);
        return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class)
            ?: [];
    }

    /**
     * Fetches and returns instance of article matching
     * specified user id representing the author and
     * article id. If such combination is not found
     * then null is returned.
     *
     * @param int $idArticle
     * @param int $idAuthor
     * @return Article|null
     */
    public function getByIds(int $idArticle, int $idAuthor) : ?Article {
        $stmt = $this->pdo->prepare("
            SELECT * FROM articles
            WHERE 
                    id_article = ?
                AND id_user_author = ?
        ");
        $stmt->execute([$idArticle, $idAuthor]);

        $result = $stmt->fetchObject(Article::class) ?: null;
        if (isset($result)){
            return $result;
        }

        $this->addErr("Článek s id $idArticle nebyl nalezen.");
    }

    /**
     * Silently removes article with matching
     * id and id of its author.
     *
     * @param int $idArticle
     * @param int $idAuthor
     */
    public function deleteByIds(int $idArticle, int $idAuthor) : void {
        $stmt = $this->pdo->prepare("
            DELETE FROM articles
            WHERE 
                    id_article = ?
                AND id_user_author = ?
        ");

        $stmt->execute([$idArticle, $idAuthor]);
    }

    /**
     * Performs validation and if it succeeds then
     * performs update of the article defined by
     * its id and id of author.
     *
     * @param array $values
     * @param int $userId
     */
    public function updateArticle(array $values, int $userId) : void {
        $this->validateArticle($values, true);
        if ($this->hasErrs()){
            return;
        }
        $stmt = $this->pdo->prepare("
            UPDATE articles
            SET title = ?, 
                abstract = ?
            WHERE id_article = ? AND id_user_author = ?
        ");

        $stmt->execute([$values['title'], $values['abstract'], $values['article-id'], $userId]);
    }

    /**
     * Performs validations and if that succeeds then
     * tries to update file of article defined by its
     * id and id of its author.
     *
     * @param array $values
     * @param int $userId
     */
    public function updateArticleFile(array $values, int $userId) : void {
        if (!$this->validateFile($values['file'])){
            return;
        }

        $fileName = $this->uploadFile($values['file'], $userId);
        if (!isset($fileName)){
            return;
        }

        $stmt = $this->pdo->prepare("
            UPDATE articles
            SET file_path = ?
            WHERE id_article = ? AND id_user_author = ?
        ");

        $stmt->execute([$fileName, $values['article-id'], $userId]);
    }
}