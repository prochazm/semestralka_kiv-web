<?php

include_once "Review.php";

/**
 * Class ReviewModel
 *
 * This is AModel implementation for
 * managing Reviews.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ReviewModel extends AModel {

    /**
     * Fetches unfinished Reviews by id of assigned
     * Reviewer and returns them as array of Reviews.
     *
     * @param int $idReviewer
     * @return array
     */
    public function getUnfinishedReviews(int $idReviewer) : array {
        $stmt = $this->pdo->prepare("
            SELECT r.* FROM reviews r, articles a
            WHERE id_user_reviewer = ?
            AND r.id_article = a.id_article
            AND a.accepted IS NULL 
        ");
        $stmt->execute([$idReviewer]);

        return
            $stmt->fetchAll(PDO::FETCH_CLASS, Review::class)
            ?: [];
    }

    /**
     * Fetches name of file uploaded with
     * Article defined by its id.
     *
     * @param int $idArticle
     * @return null|string
     */
    public function getArticleFileName(int $idArticle) : ?string {
        $stmt = $this->pdo->prepare("
            SELECT file_path FROM articles
            WHERE id_article = ?;
        ");

        $stmt->execute([$idArticle]);
        return $stmt->fetchColumn() ?: null;
    }

    /**
     * Performs validations and updates review if
     * those are satisfied.
     *
     * @param array $values
     */
    public function saveReview(array $values) : void {
        if (!isset($values,
            $values['id'],
            $values['review-content'],
            $values['relevance'],
            $values['grammar'],
            $values['correctness'])){
            $this->addErr("Všechny položky jsou povinné");
            return;
        }
        if (!$this->isOneToFiveDigit($values['relevance']) ||
            !$this->isOneToFiveDigit($values['grammar']) ||
            !$this->isOneToFiveDigit($values['correctness'])){
            $this->addErr("Akceptované hodnocení musí být celé číslo od 1 do 5");
        } if (strlen(trim($values['review-content'])) == 0){
            $this->addErr("Do recenze je potřeba také něco napsat.");
        } if ($this->hasErrs()){
            return;
        }

        $stmt = $this->pdo->prepare( "
            UPDATE reviews
            SET content = ?, relevance = ?, grammar = ?, correctness = ?
            WHERE id_review = ?
        ");

        $stmt->execute([
            $values['review-content'],
            $values['relevance'],
            $values['grammar'],
            $values['correctness'],
            $values['id'],
        ]);
    }

    /**
     * Checks if variable is integer in range
     * of 1 to 5 inclusive and returns true if
     * so or false otherwise.
     *
     * @param $item
     * @return bool
     */
    private function isOneToFiveDigit($item) : bool {
        return ctype_digit($item) ? $item > 0 && $item <= 5 : false;
    }
}