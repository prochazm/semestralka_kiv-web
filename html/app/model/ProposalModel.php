<?php

include_once "Article.php";
include_once "Review.php";

/**
 * Class ProposalModel
 *
 * This is AModel implementation for managing
 * proposals.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ProposalModel extends AModel {

    /**
     * Returns all not yet accepted articles as
     * array of Articles.
     *
     * @return array
     */
    public function getUnacceptedArticles() : array {
        $stmt = $this->pdo->prepare("
            SELECT * FROM articles WHERE accepted IS NULL;
        ");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, Article::class)
            ?: [];
    }

    /**
     * Deletes review by its id.
     *
     * @param int $reviewId
     */
    public function removeReview(int $reviewId) : void {
        $stmt = $this->pdo->prepare("
            DELETE FROM reviews 
            WHERE id_review = ?
        ");
        $stmt->execute([$reviewId]);
        if ($stmt->rowCount() == 0){
            $this->addErr('Recenze nenalezena.');
        }
    }

    /**
     * Approves article by id. That means
     * accepted flag for the article is set
     * to one.
     *
     * @param int $id
     */
    public function approve(int $id) : void {
        $this->changeState($id, 1);
    }

    /**
     * Disapproves article by its id. That
     * means accepted flag for the article
     * is set to zero.
     *
     * @param int $id
     */
    public function disapprove(int $id) : void {
        $this->changeState($id, 0);
    }

    /**
     * Sets accepted flag of the article
     * defined by id to passed value.
     *
     * @param int $id
     * @param int $approved
     */
    public function changeState(int $id, int $approved) : void {
        $stmt = $this->pdo->prepare("
            UPDATE articles
            SET accepted = ?
            WHERE id_article = ?
        ");

        $stmt->execute([$approved, $id]);
    }

    /**
     * Performs validations and pairs reviewer
     * and article both defined by ids.
     *
     * @param array $values
     */
    public function addReviewer(array $values) : void {
        if (!isset($values, $values['reviewerId'], $values['articleId'])){
            $this->addErr('Všechny parametry you povinné');
            return;
        } if (!ctype_digit($values['reviewerId']) || !ctype_digit($values['articleId'])){
            $this->addErr('Špatný formát vstupních argumentů');
            return;
        }

        $stmt = $this->pdo->prepare("
            SELECT id_user_reviewer FROM reviews
            WHERE id_user_reviewer = ? 
            AND id_article = ?
        ");
        $stmt->execute([$values['reviewerId'], $values['articleId']]);
        if ($stmt->fetch() !== false){
            $this->addErr("Recenzent tento článek již recenzuje.");
            return;
        }

        $stmt = $this->pdo->prepare("
            INSERT INTO reviews (id_article, id_user_reviewer)
            VALUES (?, ?)
        ");
        if(!$stmt->execute([$values['articleId'], $values['reviewerId']])){
            $this->addErr("Přidání recenzenta se nezdařilo.");
        }
    }

}