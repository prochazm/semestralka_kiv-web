<?php

include_once "AModel.php";
include_once "PDOFactory.php";
include_once "User.php";
include_once "Severity.php";

/**
 * Class UserModel
 *
 * This is model class for managing Users.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class UserModel extends AModel {

    /**
     * Tries to authenticate user by email
     * address and password. Instance of
     * User is returned on success and null
     * otherwise.
     *
     * @param string $mail
     * @param string $pwd
     * @return null|User
     */
    public function authenticate(string $mail, string $pwd) : ?User {
        $user = User::fetch($mail);
        if (!$user || !password_verify($pwd, $user->getPassword())){
            $this->addErr("Špatné přihlašovací údaje");
        } elseif (!$user->isEnabled()){
            $this->addErr("Váš účet byl odstaven");
        } else {
            return $user;
        }
        return null;
    }

    /**
     * Performs validations and adds new user
     * to database if validations were satisfied.
     *
     * @param array $values
     */
    public function addUser(array $values) : void {
        // validations
        if (!isset($values, $values['mail'], $values['passwd'], $values['name'], $values['surname'])){
            $this->addErr("Všechny údaje jsou povinné");
        } if (!filter_var($values['mail'], FILTER_VALIDATE_EMAIL)) {
            $this->addErr("Vložte prosím validní emailovou adresu.");
        } if ($values['passwd'] != $values['passwdrep']) {
            $this->addErr("Hesla nejsou shodná.");
        } if (!isset($values['acceptcond'])){
            $this->addErr("Musíte souhlasit s podmínkami.");
        } if (User::fetch($values['mail'] === false)){
            $this->addErr("Uživatel se stejným mailem ji existuje");
        }

        if ($this->hasErrs()){
            return;
        }

        $stmt = $this->pdo->prepare(" 
             INSERT INTO users (password, name, surname, mail) 
             VALUE (?,?,?,?) 
        ");

        $pwdHash = password_hash($values['passwd'], PASSWORD_BCRYPT);
        $stmt->execute([$pwdHash, $values['name'], $values['surname'], $values['mail']]);
    }

    /**
     * Returns all Users as array of User
     * instances.
     *
     * @return array
     */
    public function getUsers() : array {
        $stmt = $this->pdo->prepare("
            SELECT * FROM users
        ");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, User::class)
            ?: [];
    }

    /**
     * Sets enabled flag of User defined by id.
     *
     * true = 1 = enabled
     * false = 0 = disabled
     *
     * @param int $id
     * @param bool $state
     */
    public function editEnabled(int $id, bool $state) : void {
        $stmt = $this->pdo->prepare("
            UPDATE users
            SET enabled = ?
            WHERE id_user = ?;
        ");

        $stmt->execute([(int)$state, $id]);
    }

    /**
     * Gives user a role, both are defined by their
     * ids. Returns true on success and false otherwise.
     *
     * @param int $idUser
     * @param int $idRole
     * @return bool
     */
    public function setRole(int $idUser, int $idRole) : bool {
        $stmt = $this->pdo->prepare("
            UPDATE users
            SET id_role = ?
            WHERE id_user = ?;
        ");

        $stmt->execute([$idRole, $idUser]);
        return $stmt->rowCount() > 0;
    }
}