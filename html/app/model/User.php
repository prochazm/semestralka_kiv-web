<?php

/**
 * Class User
 *
 * This class represents individual user.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class User {
    private $id_user;
    private $id_role;
    private $name;
    private $surname;
    private $mail;
    private $enabled;
    private $password;

    private $role;

    /**
     * User constructor.
     *
     * Private, use static factory methods or
     * fetchObject PDO method.
     */
    private function __construct() {}

    /**
     * Fetches user by his email address.
     *
     * @param string $mail
     * @return null|User
     */
    public static function fetch(string $mail) : ?User {
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM users WHERE mail = ?");
        $stmt->execute([$mail]);

        return $stmt->fetchObject(self::class) ?: null;
    }

    /**
     * Fetches user by his id.
     *
     * @param int $id
     * @return null|User
     */
    public static function fetchById(int $id) : ?User {
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM users WHERE id_user = ?");
        $stmt->execute([$id]);

        return $stmt->fetchObject(self::class) ?: null;
    }

    /**
     * Returns all users having role defined
     * by passed role id.
     *
     * @param int $roleId
     * @return array
     */
    public static function fetchByGroup(int $roleId) : array {
        $stmt = PDOFactory::get()->prepare("
            SELECT * FROM users WHERE id_role = ?
        ");
        $stmt->execute([$roleId]);

        return $stmt->fetchAll(PDO::FETCH_CLASS, self::class)
            ?: [];
    }

    /**
     * Getter returns user's role as instance
     * of Role class.
     *
     * @return null|Role
     */
    public function getRole() : ?Role {
        if(isset($this->role)){
            return $this->role;
        }
        $stmt = PDOFactory::get()->prepare("
            SELECT * FROM role
            WHERE id_role = ?
        ");
        $stmt->execute([$this->id_role]);

        return $stmt->fetchObject(Role::class) ?: null;
    }

    /**
     * Returns user's whole name as string.
     *
     * @return string
     */
    public function __toString() : string {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * Getter returns user's id.
     *
     * @return int
     */
    public function getId() : int {
        return $this->id_user;
    }

    /**
     * Getter returns user's role id
     *
     * @return int
     */
    public function getRoleId() : int {
        return $this->id_role;
    }

    /**
     * Getter returns user's first name.
     *
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * Getter returns user's last name.
     *
     * @return string
     */
    public function getSurname() : string {
        return $this->surname;
    }

    /**
     * Getter returns user's mail.
     *
     * @return string
     */
    public function getMail() : string {
        return $this->mail;
    }

    /**
     * Returns true if user is enabled and
     * false otherwise.
     *
     * @return bool
     */
    public function isEnabled() : bool {
        return $this->enabled;
    }

    /**
     * Returns user's password hash.
     *
     * @return string
     */
    public function getPassword() : string {
        return $this->password;
    }
}