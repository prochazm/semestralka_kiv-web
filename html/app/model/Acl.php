<?php

/**
 * Class Acl
 *
 * This class represents single item from
 * access control list.
 *
 * @since 1.12.18
 * @author Martin Procházka
 */
class Acl {

    private $id_acl;
    private $id_route;
    private $string_route;
    private $id_role;
    private $string_role;
    private $action;

    /**
     * Acl constructor.
     *
     * if init is set to true (which is default state)
     * names of route and role are fetched from database
     *
     * @param bool $init
     */
    public function __construct(bool $init = true) {
        if ($init){
            $stmt = PDOFactory::get()
                ->prepare("SELECT string_id FROM role WHERE id_role = ?");
            $stmt->execute([$this->id_role]);
            $this->string_role = $stmt->fetch()['string_id'];

            $stmt = PDOFactory::get()
                ->prepare("SELECT string_id FROM routes WHERE id_route = ?");
            $stmt->execute([$this->id_route]);
            $this->string_route = $stmt->fetch()['string_id'];
        }
    }

    /**
     * Fetches whole Acl and returns it as
     * array of Acl objects.
     *
     * @return array
     */
    public static function fetchAll() : array {
        $stmt = PDOFactory::get()
            ->prepare("SELECT * FROM acl ORDER BY id_role,id_route");
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS, self::class)
            ?: [];
    }

    /**
     * Checks if passed combination of role,
     * route and action (can be omitted, default
     * is empty) is contained in Acl and returns
     * true if so and false otherwise.
     *
     * @param int $roleId id of role
     * @param string $route route name
     * @param string $action action name
     * @return bool true if such Ac exists
     */
    public static function isAllowed(int $roleId, string $route, string $action = '') : bool {
        $stmt = PDOFactory::get()->prepare("
          SELECT * FROM acl WHERE 
            id_role = ?
            and action = ? 
            and id_route = (SELECT id_route FROM routes WHERE string_id = ?)"
        );
        $stmt->execute([$roleId, $action, $route]);

        return (boolean)$stmt->fetch();
    }

    /**
     * Returns name of role contained in this
     * acl rule.
     *
     * @return string role name
     */
    public function getRole() : string {
        return $this->string_role;
    }

    /**
     * Returns name of route contained in this
     * acl rule.
     *
     * @return string route name
     */
    public function getRoute() : string {
        return $this->string_route;
    }

    /**
     * Returns action name contained in this
     * acl rule.
     *
     * @return string action name
     */
    public function getAction() : string {
        return $this->action;
    }

    /**
     * Returns id of this Ac rule.
     *
     * @return int id of this ac rule
     */
    public function getId() : int {
        return $this->id_acl;
    }
}