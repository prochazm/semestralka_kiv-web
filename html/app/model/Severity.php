<?php

/**
 * Class Severity
 *
 * This class contains few macros to be
 * used as severities for ActionResult.
 *
 * These map directly onto bootstrap
 * colors.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class Severity {
    public const SUCCESS = "success";
    public const WARNING = "warning";
    public const ERROR   = "danger";
    public const INFO    = "primary";
}