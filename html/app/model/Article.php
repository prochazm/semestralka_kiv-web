<?php

include_once "Review.php";

/**
 * Class Article
 *
 * @since 2.12.2018
 * @author Martin Procházka
 */
class Article {
    private $id_article;
    private $id_user_author;
    private $title;
    private $abstract;
    private $file_path;
    private $accepted;

    private $user;
    private $reviews;
    private $finishedReviews;
    private $possibleReviewers;

    /**
     * Article constructor.
     *
     * Private, use factory method
     */
    private function __construct() {}

    /**
     * Getter returns author of this article
     * as instance of User.
     *
     * @return User
     */
    public function getUser() : User {
        if (!isset($this->user)){
            $this->user = User::fetchById($this->getIdAuthor());
        }
        return $this->user;
    }

    /**
     * Returns array of {@link Review} instances associated
     * with this Article.
     *
     * @return array of Reviews for this Article
     */
    public function getReviews() : array {
        if (!isset($this->reviews)){
            $this->reviews = Review::FetchByArticleId($this->id_article);
        }
        return $this->reviews;
    }

    /**
     * This is similar to getReviews method except these Reviews
     * are guaranteed to have all fields filled.
     *
     * @return array of finished Reviews for this Article
     */
    public function getFinishedReviews() : array {
        if (!isset($this->reviews)){
            $stmt = PDOFactory::get()->prepare("
                SELECT r.* FROM articles a, reviews r 
                WHERE a.id_article = ?
                AND a.id_article = r.id_article
                AND r.correctness IS NOT NULL
                AND r.grammar IS NOT NULL 
                AND r.relevance IS NOT NULL
                AND r.content IS NOT NULL               
            ");
            $stmt->execute([$this->id_article]);
            $this->finishedReviews = $stmt->fetchAll(PDO::FETCH_CLASS, Review::class) ?: [];
        }
        return $this->finishedReviews;
    }

    /**
     * Finds possible reviewers and returns them
     * as array of User class instances. Possible
     * reviewer is such user that:
     *
     * 1. has review route associated with him
     * 2. is not author of the this Article
     *
     * @return array of Users
     */
    public function getPossibleReviewers() : array {
        if (isset($this->possibleReviewers)){
            return $this->possibleReviewers;
        }
        $stmt = PDOFactory::get()->prepare("
            SELECT DISTINCT id_role 
            FROM acl 
            WHERE id_route = (
                SELECT id_route FROM routes WHERE string_id = 'review'
            )
        ");
        $stmt->execute();

        $result = [];
        while(($roleId = $stmt->fetch()) !== false){
            $stmt2 = PDOFactory::get()->prepare("
                SELECT * FROM users
                WHERE id_role = ?
                AND id_user NOT IN (
                    SELECT id_user_reviewer FROM reviews 
                    WHERE id_article = ?
                )
            ");

            $stmt2->execute([$roleId['id_role'], $this->id_article]);
            $result = array_merge($result, $stmt2->fetchAll(PDO::FETCH_CLASS, User::class));
        }
        return $this->possibleReviewers = $result ?: [];
    }

    /**
     * Getter returns id of this Article
     * instance.
     *
     * @return int id of this Article
     */
    public function getId() : int {
        return $this->id_article;
    }

    /**
     * Getter returns is of Author of
     * this Article instance.
     *
     * @return int of Article Author
     */
    public function getIdAuthor() : int {
        return $this->id_user_author;
    }

    /**
     * Getter returns title of this
     * Article instance.
     *
     * @return string title of this Article
     */
    public function getTitle() : string {
        return $this->title;
    }

    /**
     * Getter returns Abstract of this
     * Article instance.
     *
     * @return string abstract of this Article
     */
    public function getAbstract() : string {
        return $this->abstract;
    }

    /**
     * Getter returns fs name of this
     * Article's attachment.
     *
     * @return string attachment name
     */
    public function getFilePath() : string {
        return $this->file_path;
    }

    /**
     * Returns true if this Article was
     * accepted and false otherwise.
     *
     * @return bool true if accepted
     */
    public function isAccepted() : bool {
        return $this->accepted === 1;
    }

    /**
     * Returns true if this Article was
     * denied and false otherwise.
     *
     * @return bool true if denied
     */
    public function isDenied() : bool {
        return $this->accepted === 0;
    }

    /**
     * Returns true if this Article is
     * waiting for verdict.
     *
     * @return bool true if not rated yet
     */
    public function isQueued() : bool {
        return $this->accepted === null;
    }
}