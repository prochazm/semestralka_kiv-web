<?php

/**
 * Class AController
 *
 * This class is abstract base of
 * each controller.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
abstract class AController{

    /**
     * AModel instance assigned to this specific
     * instance of Controller.
     *
     * @var AModel
     */
    protected $model;

    /**
     * Results of performed actions represented
     * by array of {@link ActionResult} instances.
     *
     * @var array
     */
    private $results;

    /**
     * AController constructor.
     *
     * Takes reference to AModel instance to be
     * used with concrete instance of this
     * AController.
     *
     * @param $model
     */
    function __construct(AModel $model) {
        $this->model = $model;
        $this->results = [];
    }

    /**
     * Returns array of ActionResult instances
     *
     * @see ActionResult
     * @return array of results
     */
    function getResults() : array {
        return $this->results;
    }

    /**
     * Adds single ActionResult to results.
     *
     * @param ActionResult $result
     */
    function addResult(ActionResult $result) : void {
        array_push($this->results, $result);
    }

    /**
     * Merges array of ActionResults into
     * results already present.
     *
     * @param array $results
     */
    protected function addResults(array $results) : void {
        $this->results = array_merge($this->results, $results);
    }

    /**
     * Initiates download of file by path relative to
     * uploads directory. This is considered unsafe
     * as it allows file system traversal so use this
     * only with trusted and validated sources.
     *
     * @param string $file
     */
    protected function downloadFile(string $file) : void {
        $path = '../uploads/'.$file;

        if(!file_exists($path)){
            die('not found');
        } else {
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-Disposition: attachment; filename=$file");
            header("Content-Type: application/pdf");
            header("Content-Transfer-Encoding: binary");

            readfile($path);
        }
    }

    /**
     * Returns reference to the User logged in
     * or null if no user is logged in.
     *
     * @return null|User
     */
    protected function getUser() : ?User {
        global $_CORE;
        return $_CORE->getUser();
    }

}