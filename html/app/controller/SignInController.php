<?php

include_once "AController.php";
include_once "ActionResult.php";

/**
 * Class SignInController
 *
 * This class contains actions for handling
 * authentication.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class SignInController extends AController {

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * signIn action(post)
     *
     * Tries to sign user in by submitted password
     * and email address. If this succeeds then user
     * is redirected to index and session is initiated
     * for him. On failure error is set.
     */
    function signIn() : void {
        if (!isset($_POST['mail'], $_POST['passwd'])){
            Core::redirect("signin");
        }

        $user = $this->model->authenticate($_POST['mail'], $_POST['passwd']);
        if ($this->model->hasErrs()){
            $this->addResults($this->model->getErrs());
            return;
        }
        $_SESSION['user'] = $user;
        Core::redirect("index");
    }

    /**
     * finishSession action()
     *
     * Finishes user's session and thus logging him off.
     * Redirect to is index is performed afterwards.
     */
    function finishSession() : void {
        unset($_SESSION['user']);
        $this->addResult(
            new ActionResult('Odhlášení proběhlo úspěšně', Severity::INFO, __FUNCTION__));

        Core::redirect("index", $this->getResults());
    }
}