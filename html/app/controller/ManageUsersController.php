<?php

/**
 * Class ManageUsersController
 *
 * This class defines actions for user
 * management.
 *
 * @since 12.12.2018
 * @author Martin Procházka
 */
class ManageUsersController extends AController {

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * enable action(get)
     *
     * Enables user defined by his or her id.
     * Also sets error if id is not valid.
     */
    public function enable(){
        if (!$this->validateId('id', $_GET)){
            return;
        }

        $this->model->editEnabled($_GET['id'], true);
    }

    /**
     * disable action(get)
     *
     * Disables user defined by his or her id.
     * Also sets error if id is not valid.
     */
    public function disable(){
        if (!$this->validateId('id', $_GET)){
            return;
        }

        $this->model->editEnabled($_GET['id'], false);
    }

    /**
     * setRole action(post)
     *
     * Sets users role, both are defined by
     * ids, error is set if either of those
     * are invalid.
     */
    public function setRole(){
        if (!$this->validateId('userId', $_POST)
            || !$this->validateId('roleId', $_POST)){
            return;
        }

        if ($this->model->setRole($_POST['userId'], $_POST['roleId'])){
            $this->addResult(new ActionResult("Role byla úspěšně změněna",
            Severity::SUCCESS, __FUNCTION__));
        } else {
            $this->addResult(new ActionResult("Role byla nebyla změněna",
                Severity::ERROR, __FUNCTION__));
        }
    }

    /**
     * Checks if value associated with key
     * in array is valid id.
     *
     * @param string $id key
     * @param array $source
     * @return bool true if it's valid id
     */
    private function validateId(string $id, array $source) : bool{
        if (!isset($source[$id]) || !ctype_digit($source[$id])){
            $this->addResult(new ActionResult("Je potřeba zadat validní id.",
                Severity::ERROR, __FUNCTION__));
            return false;
        }
        return true;
    }
}