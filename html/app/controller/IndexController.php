<?php

/**
 * Class IndexController
 *
 * This class defines actions for
 * index route.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class IndexController extends AController {

    /**
     * @var ReadonlyGeneralModel
     */
    protected $model;

    /**
     * download action(get)
     *
     * Initiates download of article by
     * its id.
     */
    function download(){
        if(!isset($_GET['id']) || !ctype_digit($_GET['id'])){
            $this->addResult(new ActionResult("Select article to download",
                Severity::ERROR, __FUNCTION__));
            return;
        }

        $article = $this->model->getArticle($_GET['id']);
        $this->downloadFile($article->getFilePath());
    }
}