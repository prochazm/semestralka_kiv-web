<?php

/**
 * Class ProposeController
 *
 * Defines actions related to article
 * proposition.
 *
 * @since 2.12.2018
 * @author Martin Procházka
 */
class ProposeController extends AController {

    /**
     * @var ArticleModel
     */
    protected $model;

    /**
     * propose action(post)
     *
     * Proposes article specified by title, abstract
     * and uploaded file.
     */
    public function propose() : void {
        // file count validation
        if (sizeof($_FILES) != 1){
            $this->addResult(new ActionResult(
                "Nahrajte právě jeden soubor.", Severity::ERROR, __FUNCTION__)
            );
            return;
        }

        $values = [
            'title'    => $_POST['title'],
            'abstract' => $_POST['abstract'],
            'file'     => array_values($_FILES)[0]
        ];

        $this->model->add($values);
        if ($this->model->hasErrs()){
            $this->addResults($this->model->getErrs());
            return;
        }
        $this->addResult(new ActionResult(
            "Článek byl úspěšně navržen, čekejte na vyjádření recenzentů.",
            Severity::SUCCESS,
            __FUNCTION__
        ));


    }
}