<?php

/**
 * Class SignUpController
 *
 * This class holds actions related to
 * registering new users.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class SignUpController extends AController {

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * signUp action(post)
     *
     * Tries to register user and performs
     * redirect to signin.
     */
    public function signUp() : void {
        $this->model->addUser($_POST);
        $this->addResult(new ActionResult(
                'Registrace proběhla úspěšné, nyní se můžete přihlásit',
            Severity::SUCCESS,
            __FUNCTION__
        ));
        Core::redirect("signin", $this->getResults());
    }
}