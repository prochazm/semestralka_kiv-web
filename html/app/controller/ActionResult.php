<?php

/**
 * Class ActionResult
 *
 * This class represents result of Controller
 * action and is defined by it's message and
 * severity.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class ActionResult {

    /**
     * This is field should represent
     * action where result was generated.
     *
     * @var string
     */
    private $action;

    /**
     * Message contains short explanation
     * of what happened.
     *
     * @var string
     */
    private $message;

    /**
     * Severity defines type of result such
     * as error, success or info. These are
     * predefined in {@link Severity} class.
     *
     * @see Severity
     * @var string
     */
    private $severity;

    /**
     * ActionResult constructor.
     *
     * Initiates private fields to passed
     * parameters.
     *
     * @see $action
     * @see $message
     * @see $severity
     *
     * @param string $message
     * @param string $severity
     * @param string $action
     */
    function __construct(string $message, string $severity, string $action = 'External') {
        $this->severity = $severity;
        $this->message = $message;
        $this->action = $action;
    }

    /**
     * Getter returns message.
     *
     * @return string
     */
    public function getMessage() : string {
        return $this->message;
    }

    /**
     * Getter returns severity.
     *
     * @return string
     */
    public function getSeverity() : string {
        return $this->severity;
    }

    /**
     * Getter returns triggering action.
     *
     * @return string
     */
    public function getAction() : string {
        return $this->action;
    }
}