<?php

/**
 * Class OwnArticleController
 *
 * This class defines actions user can do with
 * his own articles.
 *
 * @since 11.12.2018
 * @author Martin Procházka
 */
class OwnArticleController extends AController {

    /**
     * @var ArticleModel
     */
    public $model;

    /**
     * download action(get)
     *
     * Initiates download of a file by id of
     * article its associated with. Error is
     * set if id is not valid.
     */
    public function download() : void{
        if (!$this->validateId($_GET['id'] ?? null)){
            return;
        }

        $article = $this->model->getByIds($_GET['id'], $this->getUser()->getId());

        if (!$this->model->hasErrs()){
            $this->downloadFile($article->getFilePath());
        }
        $this->addResults($this->model->getErrs());
    }

    /**
     * delete action(get)
     *
     * Deletes article by its id.
     */
    public function delete() : void {
        if (!$this->validateId($_GET['id'] ?? null)){
            return;
        }

        $this->model->deleteByIds($_GET['id'], $this->getUser()->getId());
    }

    /**
     * edit action(POST)
     *
     * Edits article by its id. All values are basically
     * replaced by new ones except the file, if new file
     * is not present then the old one is kept.
     */
    public function edit() : void {
        $values = [
            'article-id' => $_POST['article-id'] ?? null,
            'title' => $_POST['title'] ?? null,
            'abstract' => $_POST['abstract'] ?? null,
            'pdf' => array_values($_FILES)[0] ?? null,
        ];

        $this->model->updateArticle($values, $this->getUser()->getId());

        if (isset($values['pdf']) && $values['pdf']['size'] > 0){
            $this->model->updateArticleFile($values, $this->getUser()->getId());
        }
        $this->addResults($this->model->getErrs());
    }

    /**
     * Checks if id could possibly be valid which
     * means that it is present and is a whole number.
     * Result is returned as boolean - true if id
     * might be valid and false otherwise.
     *
     * @param $id
     * @return bool
     */
    private function validateId($id) : bool {
        if (!isset($id) || !ctype_digit($id)){
            $this->addResult(new ActionResult("Id je povinný parametr.",
                Severity::ERROR, __FUNCTION__));
            return false;
        }
        return true;
    }
}