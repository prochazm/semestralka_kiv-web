<?php

/**
 * Class editAclController
 *
 * This class defines controller for
 * managing acl.
 *
 * @since 4.12.2018
 * @author Martin Procházka
 */
class editAclController extends AController {

    /**
     * @var AclModel
     */
    protected $model;

    /**
     * delete action (post)
     *
     * This action removes acl by its id
     * giving error message if id's not valid.
     */
    public function delete(){
        if(!isset($_POST['id'])){
            $this->addResult(new ActionResult(
                "Identifikátor nebyl definován, není tedy co smazat.",
                Severity::ERROR,
                __FUNCTION__
            ));
            return;
        }
        $this->model->deleteById($_POST['id']);

    }

    /**
     * add action(post)
     *
     * This action adds new ac rule defined
     * by post parameters id, role, route
     * and action.
     */
    public function add(){
        if (isset($_POST['id']) && $_POST['id'] != ''){
            $this->edit();
            return;
        }
        $values = [
            'role' => $_POST['role'] ?? null,
            'route' => $_POST['route'] ?? null,
            'action' => $_POST['action'] ?? null,
        ];

        $this->model->addAc($values);
        $this->addResults($this->model->getErrs());
    }

    /**
     * edit action(post)
     *
     * This action replaces role, route and
     * action of ac rule defined by id.
     */
    public function edit(){
        $values = [
            'id' => $_POST['id'] ?? null,
            'role' => $_POST['role'] ?? null,
            'route' => $_POST['route'] ?? null,
            'action' => $_POST['action'] ?? null,
        ];

        $this->model->updateAc($values);
        $this->addResults($this->model->getErrs());
    }
}