<?php

/**
 * Class ProposalController
 *
 * This class defines actions privileged user
 * can do with proposed articles.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ProposalController extends AController {

    /**
     * @var ProposalModel
     */
    protected $model;

    /**
     * approve action(post)
     *
     * Approves article specified by id
     */
    public function approve() : void {
        if (!$this->checkValidId()){
            return;
        }
        $this->model->approve($_POST['id']);
        $this->addResult(new ActionResult("Článek byl úspěšně schválen.", Severity::SUCCESS, __FUNCTION__));
    }

    /**
     * removeReview action(get)
     *
     * Removes review by its id.
     */
    public function removeReview() : void {
        if (!isset($_GET['reviewId']) || !ctype_digit($_GET['reviewId'])){
            $this->addResult(new ActionResult('Id článku není validní.', Severity::ERROR, __FUNCTION__));
            return;
        }

        $this->model->removeReview($_GET['reviewId']);
        $this->addResults($this->model->getErrs());
    }

    /**
     * addReviewer action(get)
     *
     * Assigns reviewer to article, both
     * are defined by their ids
     */
    public function addReviewer() : void {
        $values = [
            'articleId' => $_GET['articleId'] ,
            'reviewerId' => $_GET['reviewerId']
        ];

        $this->model->addReviewer($values);
        $this->addResults($this->model->getErrs());
    }

    /**
     * disapprove action(post)
     *
     * Denies article specified by its id.
     */
    public function disapprove() : void {
        if(!$this->checkValidId()){
            return;
        }
        $this->model->disapprove($_POST['id']);
        $this->addResult(new ActionResult("Článek byl úspěšně zamítnut.", Severity::SUCCESS, __FUNCTION__));
    }


    /**
     * Checks if id could possibly be valid
     * which means that it is set and its digit.
     *
     * @return bool true if valid
     */
    private function checkValidId() : bool {
        if(!isset($_POST['id']) || !ctype_digit($_POST['id'])){
            $this->addResult(new ActionResult('Id musí být nastaveno.', Severity::ERROR, __FUNCTION__));
            return false;
        }
        return true;
    }
}