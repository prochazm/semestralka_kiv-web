<?php

include_once "app/Core.php";
include_once "app/model/Severity.php";
include_once "app/view/AView.php";
include_once "app/model/AModel.php";
include_once "app/controller/AController.php";
include_once "app/model/Acl.php";
include_once "app/view/ProposalView.php";
include_once "app/SecureContainer.php";

/**
 * Class FrontController
 *
 * This class is simple implementation
 * of front controller pattern.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class FrontController {

    /**
     * Secured MVC route
     *
     * @var SecureContainer
     */
    private $container;

    /**
     * Contains results of performed actions.
     *
     * @var array
     */
    private $results = [];

    /**
     * FrontController constructor.
     *
     * Creates model, view and controller from
     * passed route. Router instance is needed
     * to rewrite the route for correct
     * route to classes rewrite.
     *
     * @param Router $router
     * @param string $routeName
     * @param Twig_Environment $twig
     * @throws Exception 404 if file not found
     */
    public function __construct(Router $router, string $routeName, Twig_Environment $twig) {
        $route = $router->getRoute($routeName);
        $components = [
            'model' => $route->getModel(),
            'controller' => $route->getController(),
            'view' => $route->getView(),
        ];

        foreach ($components as $key => $component){
            if (file_exists($file = "app/$key/$component.php")){
                require_once $file;
                continue;
            }
            throw new Exception('404');
        }

        $model = new $components['model']();
        $this->container = new SecureContainer(
            new $components['controller']($model),
            new $components['view']((string)$route, $model, $twig)
        );
    }

    /**
     * Performs action defined by passed
     * action name.
     *
     * @param string $action
     */
    public function doAction(string $action) : void {
        if (isset($action)){
            $this->results = array_merge($this->results, $this->container->$action());
        }
    }

    /**
     * Returns rendered view
     *
     * @return string
     * @throws Exception
     */
    public function render() : string {
        return $this->container->render();
    }

    /**
     * Returns array of ActionResults.
     *
     * @return array
     */
    public function getResults() : array {
        return $this->results ?? [];
    }
}