<?php

/**
 * Class ReviewController
 *
 * Defines actions for reviewing article.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ReviewController extends AController {

    /**
     * @var ReviewModel
     */
    protected $model;

    /**
     * download action(get)
     *
     * Initiates download of article's pdf
     * file defined by id of the article.
     */
    public function download() : void {
        if (!isset($_GET['id'])){
            echo "shit happens";
        }

        $file = $this->model->getArticleFileName($_GET['id']);
        $this->downloadFile($file);
    }

    /**
     * save action(post)
     *
     * Edits all fields of review defined
     * by its id.
     */
    public function save() : void {
        $values = [
            'review-content' => $_POST['review-content'] ?? null,
            'relevance' => $_POST['relevance'] ?? null,
            'grammar' => $_POST['grammar'] ?? null,
            'correctness' => $_POST['correctness'] ?? null,
            'id' => $_POST['review-id'] ?? null,
        ];

        $this->model->saveReview($values);
        $this->addResults($this->model->getErrs());
    }
}