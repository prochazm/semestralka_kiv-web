<?php


/**
 * Class SignInView
 *
 * This is View for sign in form.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class SignInView extends AView {

    /**
     * Just renders the sign in form and
     * returns it as string.
     *
     * @return string rendered form
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render() : string {
        return $this->twig->render("sign_in_form.html", ["route" => $this->route]);
    }
}