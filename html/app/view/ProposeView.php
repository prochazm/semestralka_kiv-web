<?php

/**
 * Class ProposeView
 *
 * This is simple View for propose form
 * template.
 *
 * @since 2.12.2018
 * @author Martin Procházka
 */
class ProposeView extends AView {

    /**
     * Renders propose form which is used
     * (obviously) for proposing new articles.
     *
     * @return string rendered propose form
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        return $this->twig->render("propose_form.html",
            [
                "route" => $this->route,
                "formAction" => "propose"
            ]
        );
    }
}