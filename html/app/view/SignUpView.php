<?php

include_once "AView.php";

/**
 * Class SignUpView
 *
 * This is view for signUp form.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class SignUpView extends AView {

    /**
     * This just renders the form, nothing
     * special about this method.
     *
     * @return string rendered sign up form
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function render() : string {
        return $this->twig->render("sign_up_form.html", ["route" => $this->route]);
    }
}