<?php

/**
 * Class IndexView
 *
 * This is the View for index page.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class IndexView extends AView {

    /**
     * @var ReadonlyGeneralModel
     */
    protected $model;

    /**
     * Renders index page with accepted articles
     * and returns it as string.
     *
     * @return string index page
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        $filling = [
            "route" => $this->route,
            "articles" => $this->model->getArticles(),
        ];

        return $this->twig->render("accepted_articles.html", $filling);
    }
}