<?php

/**
 * Class EditAclView
 *
 * This View renders table for Acl
 * management.
 *
 * @since 4.12.2018
 * @author Martin Procházka
 */
class EditAclView extends AView {

    /**
     * @var AclModel
     */
    protected $model;

    /**
     * Renders acl edit table and returns
     * it as string.
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        $filling = [
            'route' => $this->route,
            'acls' => $this->model->getAcl(),
        ];

        return $this->twig->render('acl_edit_table.html', $filling);
    }
}