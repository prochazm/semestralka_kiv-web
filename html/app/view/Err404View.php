<?php

/**
 * Class Err404View
 *
 * This is View renders 404 error page
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class Err404View extends AView {

    /**
     * Renders the 404 page and returns it
     * as string.
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        return $this->twig->render("err_404.html", ["route" => $this->route]);
    }
}