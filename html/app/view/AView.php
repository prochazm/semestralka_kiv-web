<?php

/**
 * Class AView
 *
 * This is abstract View which
 * every other View should extend.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
abstract class AView {
    /**
     * Model used to retrieve data from DB
     *
     * @var AModel
     */
    protected $model;

    /**
     * Route name
     *
     * @var string
     */
    protected $route;

    /**
     * Twig environment with template files
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * AView constructor.
     *
     * Initiates protected fields to passed arguments.
     *
     * @param string $route
     * @param $model
     * @param Twig_Environment $twig
     */
    function __construct(string $route, $model, Twig_Environment $twig) {
        $this->model = $model;
        $this->route = $route;
        $this->twig = $twig;
    }

    /**
     * This abstract method is supposed to be used
     * to retrieve content of the concrete View as
     * a string.
     *
     * @return string
     */
    abstract function render() : string;

    /**
     * Returns route of this View.
     * 
     * @return string
     */
    public function getRoute() : string {
        return $this->route;
    }
}