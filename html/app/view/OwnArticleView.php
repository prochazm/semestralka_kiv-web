<?php

/**
 * Class OwnArticleView
 *
 * This View renders table with user's
 * own articles.
 *
 * @since 11.12.2018
 * @author Martin Procházka
 */
class OwnArticleView extends AView {

    /**
     * @var ArticleModel
     */
    protected $model;

    /**
     * Renders the table and returns it as a
     * string.
     *
     * @return string rendered table
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        global $_CORE;

        $filling = [
            'route' => $this->route,
            'articles' => $this->model->getByUser($_CORE->getUser()->getId()),
        ];

        return $this->twig->render("own_articles_table.html", $filling);
    }
}