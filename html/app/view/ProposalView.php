<?php

/**
 * Class ProposalView
 *
 * This is view for proposal table. That
 * is table which is used for assigning
 * reviews to proposition and actually
 * accepting or denying the propositions.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ProposalView extends AView {

    /**
     * @var ProposalModel
     */
    protected $model;

    /**
     * Renders table of proposals and returns
     * it as a string.
     *
     * @return string rendered table
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        $filling = [
            'route' => $this->route,
            'model' => $this->model->getUnacceptedArticles(),
        ];
        return $this->twig->render("proposals_table.html", $filling);
    }
}