<?php

/**
 * Class Err403View
 *
 * This is View for 403 error page -
 * access denied.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
class Err403View extends AView {

    /**
     * Renders the page and returns it as string.
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        return $this->twig->render("err_403.html", []);
    }
}