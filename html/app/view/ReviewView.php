<?php

/**
 * Class ReviewView
 *
 * This is the View for review table template.
 *
 * @since 6.12.2018
 * @author Martin Procházka
 */
class ReviewView extends AView {

    /**
     * @var ReviewModel
     */
    protected $model;

    /**
     * Renders review table and returns it
     * as string.
     *
     * @return string rendered review table
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render(): string {
        global $_CORE;
        $userId = $_CORE->getUser()->getId();
        $reviews = $this->model->getUnfinishedReviews($userId);
        $filling = [
            'route' => $this->route,
            'reviews' => $reviews,
        ];

        return $this->twig->render("review_table.html", $filling);
    }
}