<?php

/**
 * Class ManageUsersView
 *
 * This View renders user management table.
 *
 * @since 12.12.2018
 * @author Martin Procházka
 */
class ManageUsersView extends AView {

    /**
     * @var UserModel
     */
    protected $model;

    /**
     * Render the table and returns it as string.
     *
     * @return string rendered table
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    function render() : string {
        $filling = [
            "route" => $this->route,
            "users" => $this->model->getUsers(),
            "roles" => Role::fetchAll()
        ];

        return $this->twig->render("user_management.html", $filling);
    }
}