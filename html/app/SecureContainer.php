<?php

/**
 * Class SecureContainer
 *
 * This is container for MVC triad performing
 * access validation of passed calls and
 * redirection based on validation result.
 *
 * @since 3.12.2018
 * @author Martin Procházka
 */
class SecureContainer {

    /**
     * Validated Controller
     *
     * @var AController
     */
    protected $controller;

    /**
     * Validated View
     *
     * @var AView
     */
    protected $view;

    /**
     * Core
     *
     * @var Core
     */
    protected $core;

    /**
     * Route user is trying to access.
     *
     * @var string
     */
    protected $route;

    /**
     * SecureContainer constructor.
     *
     * Sets private fields to passed parameters.
     *
     * @param AController $controller
     * @param AView $view
     */
    public function __construct(AController $controller, AView $view) {
        global $_CORE;
        $this->core = &$_CORE;
        $this->controller = $controller;
        $this->view = $view;
        $this->route = $view->getRoute();
    }

    /**
     * Tries to validate the View and then
     * renders it or throws 403 exception
     * depending on the result.
     *
     * @return string
     * @throws Exception
     */
    public function render() : string {
        if (Acl::isAllowed($this->core->getUserRole()->getId(), $this->route)){
            return $this->view->render();
        }
        throw new Exception('403');
    }

    /**
     * This is kind of a firewall standing between
     * caller and actual implementation in Controller.
     * If called method (action) in not present in
     * Controller then exception 404 is thrown, if
     * it does exist but user does not have the privilege
     * to call it then exception 403 is thrown. In
     * any other case the the method is called and
     * ActionResult array is returned.
     *
     * @see ActionResult
     * @param $method
     * @param $arguments
     * @return array
     * @throws Exception
     */
    public function __call($method, $arguments) : array {
        if (!method_exists($this->controller, $method)){
            throw new Exception('404');
        }
        if (Acl::isAllowed($this->core->getUserRole()->getId(), $this->route, $method)){
            try{
                call_user_func_array([$this->controller, $method],$arguments);
            } catch (Exception $e){
                $this->controller->addResult(new ActionResult($e->getMessage(),Severity::ERROR));
            }
            return $this->controller->getResults();
        }
        throw new Exception('403');
    }
}