<?php

/**
 * Class Core
 *
 * This class contains some Core functionality.
 *
 * @since 1.12.2018
 * @author Martin Procházka
 */
class Core {

    /**
     * Logged user
     *
     * @var null|User
     */
    private $user;

    /**
     * Reference to User's role. If user
     * is not logged in then this is
     * mapped to 'guest'
     *
     * @var null|Role
     */
    private $role;

    /**
     * Core constructor.
     *
     * Fetches private fields
     */
    public function __construct() {
        if (isset($_SESSION['user'])){
            $this->user = $_SESSION['user'];
            $this->role = Role::fetch($this->user->getRoleId());
            return;
        }
        $this->user = null;
        $this->role = Role::fetchByName('guest');
    }

    /**
     * Redirects page to different route, sets
     * results to be displayed and action to be
     * performed, the latter two can be omitted
     * as null is default value. (no results and
     * no action)
     *
     * @param string $route
     * @param array|null $results
     * @param string|null $action
     */
    public static function redirect(string $route, array $results = null, string $action = null){
        $route = "?route=$route";
        $action = isset($action) ? "&action=$action" : "";
        header("Location: $route$action");
        if (isset($results)){
            $_SESSION['action_results'] = $results;
        }
        die("Redirecting to $route$action");
    }

    /**
     * Returns logged user or null.
     *
     * @return null|User
     */
    public function getUser() : ?User {
        return $this->user;
    }

    /**
     * Returns logged user or guest.
     *
     * @return Role
     */
    public function getUserRole() : Role {
        return $this->role;
    }
}